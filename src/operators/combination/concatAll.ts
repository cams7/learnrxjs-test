import { interval, of, fromEvent } from 'rxjs';
import { take, concatAll, map, reduce, mapTo, startWith, scan } from 'rxjs/operators';

let response = {
    next: console.log,
    error: (message: any) => console.log('Error: ' + message),
    complete: () => console.log('Completed')
}

const obs1 = interval(1000).pipe(take(5), map((val: number) => 10 + val));
const obs2 = interval(500).pipe(take(2), map((val: number) => 100 + val));
const obs3 = interval(2000).pipe(take(1), map((val: number) => 1000 + val));

of(obs1, obs2, obs3).pipe(
  concatAll(),
  reduce((acc:string, val:number) => `${acc}, ${val}`)
).subscribe(response);

//-------------------------------------------------------------------------------------------------  

//const clicks$ = fromEvent(document, 'click').pipe(mapTo(1), startWith(0), scan((acc:number, curr:number) => acc + curr), take(3));
//const numbers$ = interval(1000).pipe(take(4))
//of(clicks$, numbers$).pipe(concatAll()).subscribe(response);

//-------------------------------------------------------------------------------------------------  

fromEvent(document, 'click').pipe(
    map((event:any) => interval(500).pipe(take(4))),
    concatAll(),
    //scan((acc:string, val:number) => `${acc}, ${val}`)
).subscribe(response);