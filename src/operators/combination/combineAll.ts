import { take, map, combineAll, reduce } from 'rxjs/operators';
import { interval, fromEvent, of, timer} from 'rxjs';

let response = {
    next: console.log,
    error: (message: any) => console.log('Error: ' + message),
    complete: () => console.log('Completed')
}

fromEvent(document, 'click').pipe(
    map(ev => interval(3000).pipe(
        take(3)
    )),
    //map(val1 => of(1, 2, 3, 4)),
    take(2),
    combineAll()
).subscribe(response);

//-------------------------------------------------------------------------------------------------  

of('a', 'b', 'c').pipe(
    map(val1 => of(1, 2, 3, 4).pipe(
        map(val2 => `${val1}${val2}`)
    )),
    combineAll()
).subscribe(response);

//-------------------------------------------------------------------------------------------------  

const caracteres:string[] = [ 'a', 'b', 'c', 'd', 'e', 'f', 'j', 'k', 'l','m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'y', 'x', 'z' ];

const timeOne$ = timer(1000, 3000).pipe(take(2), map((i:number) => caracteres[i]));//a(1s),              b(4s)
const timerTwo$ = timer(2000, 1000).pipe(take(2), map((i:number) => i + 1));      //        1(2s), 2(3s)
of(timeOne$, timerTwo$).pipe(
    combineAll(), 
    map(([val1, val2]) => `${val1}${val2}`),
    reduce((acc:string, val:string) => `${acc}, ${val}`)
).subscribe(response); //a1, a2, b2
