import { take, map, mapTo, startWith, scan, tap, reduce, combineAll } from 'rxjs/operators';
import { fromEvent, of, combineLatest, timer } from 'rxjs';

let response1 = {
    next: (x:string) => console.log('BMI is ' + x),
    error: (message: any) => console.log('Error: ' + message),
    complete: () => console.log('Completed 1')
}

let weight$ = of(70, 72, 76, 79, 75);
let height$ = of(1.76, 1.77, 1.78);
let bmi$ = combineLatest(weight$, height$, (w:number, h:number) => w / (h * h)).pipe(reduce((acc:string, val:number) => `${acc}, ${val}`));
bmi$.subscribe(response1);

//-------------------------------------------------------------------------------------------------

// helper function to set HTML
const setHtml = (id:string) => (val:number) => (document.getElementById(id).innerHTML = String(val));

let response2 = {
    next: setHtml('total'),
    error: (message: string) => console.log('Error: ' + message),
    complete: () => console.log('Completed 2')
}

const addOneClick$ = (id:string, total:number) =>
  fromEvent(document.getElementById(id), 'click').pipe(
    // map every click to 1
    mapTo(1),
    startWith(0),
    // keep a running total
    scan((acc:number, curr:number) => acc + curr),
    // set HTML for appropriate element
    tap(setHtml(`${id}Total`)),
    take(total)
  );

const combineTotal$ = combineLatest(addOneClick$('red', 3), addOneClick$('black', 5))
  .pipe(map(([val1, val2]) => val1 + val2))
  .subscribe(response2);

//-------------------------------------------------------------------------------------------------  

let response3 = {
    next: console.log,
    error: (message: any) => console.log('Error: ' + message),
    complete: () => console.log('Completed 3')
}

const caracteres:string[] = [ 'a', 'b', 'c', 'd', 'e', 'f', 'j', 'k', 'l','m', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'y', 'x', 'z' ];

const timeOne$ = timer(1000, 3000).pipe(take(5), map((i:number) => caracteres[i]));//a(1s),         b(4s),                           c(7s), d(10s), e(13s)
const timerTwo$ = timer(3500, 1000).pipe(take(4), map((i:number) => i + 1));        //       1(3.5s),       2(4.5s), 3(5.5s), 4(6.5s)
combineLatest(timeOne$, timerTwo$).pipe(
    map(([val1, val2]) => `${val1}${val2}`),
    reduce((acc:string, val:string) => `${acc}, ${val}`)
).subscribe(response3); //a1, b1, b2, b3, b4, c4, d4, e4

of(timeOne$, timerTwo$).pipe(
    combineAll(), 
    map(([val1, val2]) => `${val1}${val2}`),
    reduce((acc:string, val:string) => `${acc}, ${val}`)
).subscribe(response3); //a1, b1, b2, b3, b4, c4, d4, e4


