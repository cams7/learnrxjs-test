import { Observable } from 'rxjs';

let logItem = (val:any) => {
    let node = document.createElement("li");
    let textnode = document.createTextNode(val);
    node.appendChild(textnode);
    document.getElementById("list").appendChild(node);
}

let response = {
    next: (message:any) => logItem(message),
    error: (message: any) => logItem ('Error: ' + message),
    complete: () => logItem('Completed')
}

let observable = Observable.create((observer:any) => {
    observer.next('Hello World!');
    observer.next('Hello Again!');
    //observer.complete();
    observer.error("I'm sorry");
    observer.next('Bye');
})
observable.subscribe(response);
