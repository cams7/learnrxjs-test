const path = require('path');
module.exports = {
    entry: {
      "index":'./src/index.ts',
      "combineAll":'./src/operators/combination/combineAll.ts',
      "combineLatest":'./src/operators/combination/combineLatest.ts',
      "concatAll":'./src/operators/combination/concatAll.ts'
    },
    devtool: 'inline-source-map',
    module: {
      rules: [
        {
          test: /\.tsx?$/,
          use: 'ts-loader',
          exclude: /node_modules/
        }
      ]
    },
    resolve: {
      extensions: [ '.tsx', '.ts', '.js' ]
    },
    output: {
      filename: "[name].js",
      path: path.resolve(__dirname, 'dist')
    }
};